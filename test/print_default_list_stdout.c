#include <CUnit/Automated.h>
#include <CUnit/CUnit.h>
#include <CUnit/Basic.h>
#include <unistd.h>

const char* TEST_DIR_WITH_DEFAULT_LIST =
	"test/print_default_list_stdout/with_default_list";
const char* TEST_DIR_WITH_NO_LISTS =
	"test/print_default_list_stdout/with_no_lists";
// content of first line in TEST_DIR_WITH_DEFAULT_LIST/.list/todo.md
const char* TEST_DEFAULT_LIST_CONTENT = "DEFAULT LIST";

const char* NO_DEFAULT_LIST_MSG = "current working directory does not contain a default "
	"list file. Are you in the correct directory? If yes, "
	"then initialize your lists by creating the first and "
	"default one '.list/todo.md'.";

char originalDir[512];

int suit_initialize ( void )
{
	getcwd(originalDir, 512);
	return CUE_SUCCESS;
}

void move_to_test_case_dir(const char* dir) {
	chdir(dir);
}

void move_to_original_dir() {
	chdir(originalDir);
}

void test_print_default_list_to_stdout( void )
{
	move_to_test_case_dir(TEST_DIR_WITH_DEFAULT_LIST);

	FILE *list_output_buffer = popen("list", "r");

	if ( list_output_buffer == NULL ) {
		if ( errno != 0 ) {
			perror("Failed to execute list");
		}
		CU_FAIL("Failed to test printing default list to stdout.");
	}

	char list_output[13];
	fgets(list_output, 13, list_output_buffer);

	CU_ASSERT_STRING_EQUAL(list_output, TEST_DEFAULT_LIST_CONTENT);

	if ( list_output_buffer != NULL) {
		pclose(list_output_buffer);
	}

	move_to_original_dir();
}

void test_print_help_msg_when_no_default_list_exist( void )
{
	move_to_test_case_dir(TEST_DIR_WITH_NO_LISTS);

	FILE *list_output_buffer = popen("list", "r");

	if ( list_output_buffer == NULL ) {
		if ( errno != 0 ) {
			perror("Failed to execute list");
		}
		CU_FAIL("Failed to test printing default list to stdout.");
	}

	char list_output[190];
	fgets(list_output, 190, list_output_buffer);

	CU_ASSERT_STRING_EQUAL(list_output, NO_DEFAULT_LIST_MSG);

	if ( list_output_buffer != NULL) {
		pclose(list_output_buffer);
	}

	move_to_original_dir();
}

int suit_cleanup ( void )
{
	return CUE_SUCCESS;
}

int main ( void )
{
	CU_initialize_registry();

	CU_pSuite list_test_suit = CU_add_suite(
						 "list",
						 &suit_initialize,
						 &suit_cleanup
						 );

	CU_add_test(
		    list_test_suit,
		    "test print default list to stdout",
		    &test_print_default_list_to_stdout
		    );

	CU_add_test(
		    list_test_suit,
		    "test print help message when no default list exist in working directory",
		    &test_print_help_msg_when_no_default_list_exist
		    );

	CU_set_output_filename("PrintDefaultListToSTDOUT");

	CU_list_tests_to_file();
	CU_automated_run_tests();
	CU_basic_run_tests();

	CU_cleanup_registry();
}
