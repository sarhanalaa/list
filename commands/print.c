#ifdef PROGRAM_LIST

int list_print()
{
	FILE *default_list_file = fopen(".list/todo.md", "r");
	if (!default_list_file) {
		printf("current working directory does not contain a default "
		       "list file. Are you in the correct directory? If yes, "
		       "then initialize your lists by creating the first and "
		       "default one '.list/todo.md'.");
	} else {
		char buffer[1024];
		while (fgets(buffer, 1024, default_list_file) != NULL) {
			fputs(buffer, stdout);
		}
		fclose(default_list_file);
	}

	return 0;
}

#endif
