#ifdef PROGRAM_LIST

int list_edit_open_editor(char* filename)
{
	size_t command_length = strlen(TXT_EDITOR) + strlen(filename) + 1;
	char command[command_length];

	sprintf(command, "%s %s", TXT_EDITOR, filename);

	return system(command);
}

int list_edit()
{
	FILE *default_list_file = fopen(".list/todo.md", "r");
	if (!default_list_file) {
		printf("current working directory does not contain a default "
		       "list file. Are you in the correct directory? If yes, "
		       "then initialize your lists by creating the first and "
		       "default one '.list/todo.md'.");

		return 0;
	} else {
		fclose(default_list_file);

		return list_edit_open_editor(".list/todo.md");
	}
}

#endif
