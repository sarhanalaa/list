#ifndef PROGRAM_LIST
#define PROGRAM_LIST								1

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <argp.h>
#include <tools.h>
#include "commands/commands.h"
#include "commands/print.c"
#include "commands/edit.c"

static char doc[] =
	"List - manage generic lists for your productivity";

static char args_doc[] = "";

struct arguments {
	int cmd;
};

/* The options we understand. */
static struct argp_option options[] = {
	{"edit",  'e', 0, 0,  "open text editor with list in edit mode" },
	{0}
};

static error_t
parse_opt (int key, char *arg, struct argp_state *state)
{
	struct arguments *args = state->input;

	switch (key)
		{
		case 'e':
			args->cmd = LIST_COMMAND_EDIT;
			break;
		default:
			return ARGP_ERR_UNKNOWN;
		}
	return 0;
}

static struct argp argp = { options, parse_opt, args_doc, doc };

struct arguments program_parse_args(int argc, char **argv)
{
	struct arguments args =
		{
			LIST_COMMAND_PRINT
		};

	argp_parse(&argp, argc, argv, 0, 0, &args);

	return args;
}

int program_execute_command(int command)
{
	switch(command) {
	case LIST_COMMAND_EDIT:
		list_edit();
		break;
	case LIST_COMMAND_PRINT:
	default:
		list_print();

	}
}

int main(int argc, char **argv)
{
	struct arguments args = program_parse_args(argc, argv);

	return program_execute_command(args.cmd);
}

#endif
